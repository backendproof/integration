package com.myproject.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myproject.integration.camel.route.ProductRoute;
import com.myproject.model.GroupResponse;
import org.apache.camel.*;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.builder.ExchangeBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class IntegrationApplicationTests extends CamelTestSupport {

	private String response;

	@Override
	protected RoutesBuilder createRouteBuilder() {
		return new ProductRoute();
	}

	public void setUp() throws Exception {
		super.setUp();
		final InputStream in = getClass().getClassLoader().getResourceAsStream("response.json");
		this.response = convert(in, StandardCharsets.UTF_8);
	}

	private String convert(InputStream inputStream, Charset charset) throws IOException {

		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, charset))) {
			return br.lines().collect(Collectors.joining(System.lineSeparator()));
		}
	}

	@Test
	public void testHeaders() throws Exception {

		//Arrange
		RouteDefinition routeDefinition = context.getRouteDefinition(ProductRoute.ROUTE_ID);
		routeDefinition.adviceWith(context, new AdviceWithRouteBuilder() {
			@Override
			public void configure() throws Exception {

				interceptSendToEndpoint("https4:*")
						.skipSendToOriginalEndpoint()
						.to("mock:http4")
						.process(exchange ->  {
							exchange.getIn().setBody(response);
						});
			}
		});

		MockEndpoint mockEndpointHttp4 = getMockEndpoint("mock:http4");
		Object request = null;

		// Headers
		Exchange entryExchange = ExchangeBuilder.anExchange(context).build();

		mockEndpointHttp4.expectedHeaderReceived(Exchange.HTTP_METHOD, RequestMethod.GET);
		// Headers

		// Act
		String response = template.requestBody("direct:groupproduct", request, String.class);

		ObjectMapper mapper = new ObjectMapper();
		GroupResponse groupResponse = mapper.readValue(response, GroupResponse.class);

		// Assert
		mockEndpointHttp4.assertIsSatisfied();
		assertEquals(4, groupResponse.getBankProducts().size());
	}

}

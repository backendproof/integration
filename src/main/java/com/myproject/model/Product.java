
package com.myproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "Sub-category",
    "Landing-page"
})
public class Product {

    @JsonProperty("name")
    private String name;
    @JsonProperty("Sub-category")
    private String subCategory;
    @JsonProperty("Landing-page")
    private String landingPage;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Sub-category")
    public String getSubCategory() {
        return subCategory;
    }

    @JsonProperty("Sub-category")
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    @JsonProperty("Landing-page")
    public String getLandingPage() {
        return landingPage;
    }

    @JsonProperty("Landing-page")
    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

}

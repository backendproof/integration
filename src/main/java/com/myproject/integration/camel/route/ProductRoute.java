package com.myproject.integration.camel.route;

import com.myproject.integration.camel.processor.PosProductsProcessor;
import com.myproject.model.GroupResponse;

import io.apigee.openbank.Products;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProductRoute extends RouteBuilder {

    @Value("${endpoint.products.host}")
    private String host = "apis-bank-test.apigee.net";

    @Value("${endpoint.products.resource}")
    private String resource = "/apis/v2.0.1/products";

    public static final String ROUTE_ID = "route-id";

    @Override
    public void configure() throws Exception {

        rest("/api/")
                .get("/group")
                .to("direct:groupproduct");

        from("direct:groupproduct")
                .routeId(ROUTE_ID)
                .log(LoggingLevel.INFO, "Begin")
                .removeHeaders("CamelHttp*", "CamelHttp(Method|Path|Query)")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.HTTP_PATH, simple(resource))
                .to("https4:" + host)
                .unmarshal().json(JsonLibrary.Jackson, Products.class)
                .process(new PosProductsProcessor())
                .marshal().json(JsonLibrary.Jackson, GroupResponse.class)
                .end();

    }
}

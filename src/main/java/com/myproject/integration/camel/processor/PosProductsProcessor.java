package com.myproject.integration.camel.processor;

import static java.util.stream.Collectors.groupingBy;

import com.myproject.model.GroupResponse;
import com.myproject.model.Product;
import io.apigee.openbank.BankProduct;
import io.apigee.openbank.Products;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PosProductsProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(PosProductsProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {

        LOGGER.info("Entro a process");
        Products products = exchange.getIn().getBody(Products.class);

        //GroupBy Category
        Map<String, List<BankProduct>> productPerType = products.getData().getBankProducts()
                .stream()
                .collect(groupingBy(BankProduct::getCategory));

        GroupResponse response = new GroupResponse();
        List<com.myproject.model.BankProduct> bankProductList = new ArrayList<>();

        for(String category : productPerType.keySet()) {
            List<BankProduct> productInCategory = productPerType.get(category);
            com.myproject.model.BankProduct productCategory = new com.myproject.model.BankProduct();
            productCategory.setCategory(category);
            productCategory.setProducts(new ArrayList<>());
            for(BankProduct product : productInCategory) {
                Product modelProduct = new Product();
                modelProduct.setName(product.getName());
                modelProduct.setSubCategory(product.getSubCategory());
                modelProduct.setLandingPage(product.getLandingPage());
                productCategory.getProducts().add(modelProduct);
            }

            bankProductList.add(productCategory);
        }

        response.setBankProducts(bankProductList);

        exchange.getOut().setBody(response, GroupResponse.class);
    }
}

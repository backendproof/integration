
package io.apigee.openbank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "Category",
    "Sub-category",
    "More-info",
    "Landing-page"
})
public class BankProduct {

    @JsonProperty("name")
    private String name;
    @JsonProperty("Category")
    private String category;
    @JsonProperty("Sub-category")
    private String subCategory;
    @JsonProperty("More-info")
    private MoreInfo moreInfo;
    @JsonProperty("Landing-page")
    private String landingPage;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("Category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("Sub-category")
    public String getSubCategory() {
        return subCategory;
    }

    @JsonProperty("Sub-category")
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    @JsonProperty("More-info")
    public MoreInfo getMoreInfo() {
        return moreInfo;
    }

    @JsonProperty("More-info")
    public void setMoreInfo(MoreInfo moreInfo) {
        this.moreInfo = moreInfo;
    }

    @JsonProperty("Landing-page")
    public String getLandingPage() {
        return landingPage;
    }

    @JsonProperty("Landing-page")
    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

}

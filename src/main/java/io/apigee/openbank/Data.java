
package io.apigee.openbank;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "BankProducts"
})
public class Data {

    @JsonProperty("BankProducts")
    private List<BankProduct> bankProducts = null;

    @JsonProperty("BankProducts")
    public List<BankProduct> getBankProducts() {
        return bankProducts;
    }

    @JsonProperty("BankProducts")
    public void setBankProducts(List<BankProduct> bankProducts) {
        this.bankProducts = bankProducts;
    }

}

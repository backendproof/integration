
package io.apigee.openbank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Maximum-tenure",
    "Rate-of-interest"
})
public class MoreInfo {

    @JsonProperty("Maximum-tenure")
    private Integer maximumTenure;
    @JsonProperty("Rate-of-interest")
    private Integer rateOfInterest;

    @JsonProperty("Maximum-tenure")
    public Integer getMaximumTenure() {
        return maximumTenure;
    }

    @JsonProperty("Maximum-tenure")
    public void setMaximumTenure(Integer maximumTenure) {
        this.maximumTenure = maximumTenure;
    }

    @JsonProperty("Rate-of-interest")
    public Integer getRateOfInterest() {
        return rateOfInterest;
    }

    @JsonProperty("Rate-of-interest")
    public void setRateOfInterest(Integer rateOfInterest) {
        this.rateOfInterest = rateOfInterest;
    }

}
